# tabix Singularity container
Bionformatics package tabix<br>
A set of tools written in Perl and C++ for working with VCF files.
https://sourceforge.net/projects/samtools
tabix Version: 0.2.6<br>

Singularity container based on the recipe: Singularity.tabix_v0.2.6

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull tabix_v0.2.6.sif oras://registry.forgemia.inra.fr/gafl/singularity/tabix/tabix:latest


